#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <iostream>
#include <string>
#include <vector>
#include "track.h"
using namespace std;

/** \class Playlist
 */

class Playlist
{
  //Propriétés
 private:
  unsigned int id;///< Object identifier
  int duration;///< Playlist wanted duration (could be different from the one computed)
  vector<Track> myTrack; ///< List of #audio::Track. Actually the core of the playlist
  
  public:

//méthodes


  //Constructeurs
  //Constructeurs/destructeur

  /** \brief Default constructor
   *
   * Construct a blank playlist object.
   */

  Playlist();

   /** \brief Detailed constructor
   *
   *
   * \param[in] _duration Duration you want the playlist to be long as
   *
   * \param[in] _name The name of your playlist (default to \c playlist)
   *
   * \param[in] _description Playlist text description (default to empty string)
   */

  Playlist(int, int, vector<Track>);

  
  Playlist (int,int, Track);
  ~Playlist();


  unsigned int getId();
  std::vector<Track> getTrack();
  int getDuration();
  void setDuration(int);
  void setTrack(vector<Track>);

};
#endif //PLAYLIST_H
