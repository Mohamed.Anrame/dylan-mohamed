#include <iostream>
#include "../header/album.h"


using namespace std;


//Implémentation du constructeur par défaut 
Album::Album():

  id(0),
  date(),
  name("")

  
{
  cout<<"création d'un album";
}

Album::Album(int _id,
             std::tm _date,
             std::string _name):
  id(_id),
  date(_date),
  name(_name)

  
{
  cout<<"création d'un album avec paramètres";
}

Album::~Album(){};

int Album::getId(){
  return id;
}

std::tm Album::getDate(){
   return date;
}

std::string Album::getName(){
  return name;
}

void Album::setDate(std::tm _date){
  _date = date;
}

void Album::setName(std::string _name) {
  _name=name; 
    }
