#include <iostream>
#include "../header/genre.h"

using namespace std;


//Implémentation du constructeur par défaut 

Genre::Genre():

    id(0),
    libelle("")

    {
        cout <<"Création d'un genre";
    }

    Genre::Genre(int _id,
                std::string _libelle):
id(_id),
libelle(_libelle)

{
    cout <<"Création d'un genre avec paramètres";
}

Genre::~Genre(){};

int Genre::getId(){
    return id;
}

std::string Genre::getLibelle(){
  return libelle;
}
